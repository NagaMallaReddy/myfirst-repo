# MSI keys , add this details to backend.tf file if there is any change in value
variable "subscription_id" {
  description = "Subscription ID"
  default     = "f9426ed1-3cf5-461f-be58-f0a39e07599e"
}
variable "client_id" {
  description = "App Id"
  default     = "8dc31480-18ae-4b03-832d-132c008232fc"
}
variable "client_secret" {
  description = "Key for Service principal"
  default     = "0V1RtqbnB5mgE~CTNB9m-VLR6X7axO30ni"
}
variable "tenant_id" {
  description = "Tenant ID from AD"
  default     = "687f51c3-0c5d-4905-84f8-97c683a5b9d1"
}

variable "rgname" {
  default     = "naga"
}

variable "rglocation" {
  default     = "West Europe"
}


# VNet variables
variable "vnet_name" {
  description = "Virtual network name"
  default     = "TFVnet"
}
variable "vnet_address_space" {
  description = "VNet address space"
  default     = ["10.0.0.0/16"]
}


#Subnet variables
variable "subnet_name" {
  description = "Subnet name"
  default     = "tfsubnet"
}
variable "subnet_address_prefixes" {
  description = "subnet address prefixes"
  default     = ["10.0.1.0/24"]
}

# Network security group variables
variable "nsgname" {
  description = "Network security group name "
  default     = "TFNSG"
}


#Load balancer variables
variable "lbipname" {
  description = "Load balancer IP name"
  default ="TFPublicIPForLB"
}
variable "lbname" {
  description = "Load balancer name"
  default="TFLoadBalancer"
}