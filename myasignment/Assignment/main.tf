module "resource_group" {
  
  source     = "./modules/resource_group"
  resgrpname = var.resgrpname
  rglocation = "east us"
}

module "virtual_network" {
  
  source             = "./modules/virtual_network"
  resgrpname         = module.resource_group.rg.name
  rglocation         = module.resource_group.rg.location
  vnet_name          = var.vnet_name
  vnet_address_space = var.vnet_address_space
}

module "subnet" {
  
  source                  = "./modules/subnet"
  resgrpname              = module.resource_group.rg.name
  rglocation              = module.resource_group.rg.location
  subnet_name             = "subnet"
  vnet_name               = module.virtual_network.vnet.name
  subnet_address_prefixes = var.subnet_address_prefixes
}

module "security_group" {
  
  source     = "./modules/security_group"
  resgrpname = module.resource_group.rg.name
  rglocation = module.resource_group.rg.location
  nsgname    = "mynsg"
}

module "lb" {
  
  source     = "./modules/load_balancer"
  name = "mypubip"
  location = module.resource_group.rg.location
  rgname = module.resource_group.rg.name
  lbname = "mylb"
}

module "vmss" {
  
  source     = "./modules/vmss"
  resource_group_name = module.resource_group.rg.name
  loadbalancer_id = module.lb.lb.id
  name = "myvmss"
  location = module.resource_group.rg.location
  subnet_id = module.subnet.subnet.id
}

module "appserver" {
  
  source     = "./modules/appserver"
  name                = "nagaplan"
  location            = module.resource_group.rg.location
  resource_group_name = module.resource_group.rg.name
  appname             = "tfsurajnagaapp"
}

